import DataSourceContext, { withDataSource } from "./context";
import DataSource from "./datasource";

export default DataSource;

export { DataSourceContext, withDataSource };

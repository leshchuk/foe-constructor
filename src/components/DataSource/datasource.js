import firebase from "../Firebase/firebase";

class DataSource {
  constructor() {
    this.firebase = firebase;
    this.subscriptions = {};
  
    this.add = async (path, value) => {
      try {
        const ref = await this.firebase.db.ref(path).push();
        const uid = ref.getKey();
        const result = await this.firebase.db.ref(`${path}/${uid}`).set({...value, uid });
        return !!result;
      } catch (error) {
        return error;
      }
    }
  
    this.update = async (path, values = {}) => {
      try {
        const result = await this.firebase.db.ref(path).update(values)
        return !!result;
      } catch (error) {
        return error;
      }
    }
  
    this.delete = async (path) => {
      try {
        const result = await this.firebase.db.ref(path).set(null)
        return !!result;
      } catch (error) {
        return error;
      }
    }
  }

  subscribe(path, callback, errorCallback) {
    this.unsubscribe(path);
    this.subscriptions[path] = this.firebase.db.ref(path);
    return this.subscriptions[path].on("value", callback, errorCallback);
  }

  unsubscribe(path) {
    if (this.subscriptions[path]) {
      this.subscriptions[path].off("value");
      delete this.subscriptions[path];
    }
  }
}

export default new DataSource();

import React from "react";

import dataSource from "./datasource";

const DataSourceContext = React.createContext(dataSource);

export const withDataSource = Component => props => (
  <DataSourceContext.Consumer>
    {datasourse => <Component {...props} datasourse={datasourse} />}
  </DataSourceContext.Consumer>
);

export default DataSourceContext;

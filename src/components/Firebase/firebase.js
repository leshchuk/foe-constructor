import app from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/functions";
import "firebase/storage";
import "firebase/firestore";

const config = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_APP_ID,
  measurementId: process.env.REACT_APP_MEASUREMENT_ID,
};
console.log("FIREBASE CONFIG", JSON.stringify(config, null, 2));

class Firebase {
  constructor() {
    app.initializeApp(config);

    this.auth = app.auth();
    this.db = app.database();
    this.cfs = app.firestore();
    this.functions = app.functions();
    this.storage = app.storage();
  }

  // *** Merge Auth and DB User API *** //

  onAuthUserListener = (next, fallback) =>
    this.auth.onAuthStateChanged((authUser) => {
      /*if (authUser) {
        this.authUserRef = this.db.ref(`/users/${authUser.uid}`);
        this.authUserRef.once("value").then((snapshot) => {
          const dbUser = snapshot.val() || {};
          console.log("LISTENER:USER", dbUser);

          // default empty roles
          if (!dbUser.roles) {
            dbUser.roles = {};
          }

          // merge auth and db user
          authUser = {
            uid: authUser.uid,
            email: authUser.email,
            creationTime: authUser.metadata.creationTime,
            lastSignInTime: authUser.metadata.lastSignInTime,
            ...dbUser,
          };
          console.log("LISTENER:AUTHUSER", authUser);

          console.log("AUTH USER", authUser);
          next(authUser);
          this.authUserRef.on("value", (snap) => {
            const user = snap.val();
            if (user) next(user);
          });
        });
      } else {
        if (this.authUserRef) {
          this.authUserRef.off();
          this.authUserRef = null;
        }
        fallback();
      }*/
    });

  // *** Auth API ***

  doCreateUserWithEmailAndPassword = (email, password) =>
    this.auth.createUserWithEmailAndPassword(email, password);

  doSignInWithEmailAndPassword = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

  doSignOut = () => this.auth.signOut();

  doPasswordReset = (email) => this.auth.sendPasswordResetEmail(email);

  doPasswordUpdate = (password) =>
    this.auth.currentUser.updatePassword(password);

  eras = () => new Promise((resolve, reject) => {
    this.db.ref("eras")
      .orderByChild("order")
      .once("value")
      .then(snap => {
        let data = Object.values(snap.val() || {})
        resolve(data.sort((a, b) => parseInt(a.order) - parseInt(b.order)));
      })
      .catch(err => {
        reject(err);
      });
  });
  
  erasSubscription = () => this.db.ref("eras")
    .orderByChild("order")
    .once("value");
  getEra = (uid) => new Promise((resolve, reject) => {
    this.db.ref(`eras/${uid}`)
      .once("value")
      .then(snap => {
        let data = snap.val() || {}
        resolve(data);
      })
      .catch(err => {
        reject(err);
      });
  });
  setEra = (data) => new Promise((resolve, reject) => {
    const ref = this.db.ref(`eras`);
    const {uid = ref.push()} = data;
    this.db.ref(`eras/${uid}`)
      .set({ ...data, uid })
      .then(() => {
        resolve({ ...data, uid });
      })
      .catch(err => {
        reject(err);
      });
  });
  delEra = (uid) => new Promise((resolve, reject) => {
    this.db.ref(`eras/${uid}`)
      .set(null)
      .then(() => {
        resolve(true);
      })
      .catch(err => {
        reject(err);
      });
  });

  // *** User API ***

  // users = (list) => {
  //   if (list && list instanceof Array) {
  //     return new Promise((resolve, reject) => {
  //       Promise.allSettled(
  //         list.map((user) => this.user(user.uid || user))
  //       ).then((results) => {
  //         resolve(
  //           results.map((result) =>
  //             result.status === "fulfilled" ? result.value : null
  //           )
  //         );
  //       });
  //     });
  //   } else {
  //     return new Promise((resolve, reject) => {
  //       let users = [];
  //       this.usersList()
  //         .then((result) => {
  //           users = result.data.users;
  //           return this.db.ref("users").once("value");
  //         })
  //         .then((snapshot) => {
  //           const data = snapshot.val();
  //           users.forEach((v, i, a) => {
  //             const user = data[v.id];
  //             if (user) {
  //               a[i] = { ...v, ...user };
  //             }
  //           });
  //           resolve(users);
  //         })
  //         .catch((error) => {
  //           reject(error);
  //         });
  //     });
  //   }
  // };

  // user = (uid) => {
  //   return new Promise((resolve, reject) => {
  //     Promise.all([
  //       this.functions.httpsCallable("user")(uid),
  //       this.db.ref(`users/${uid}`).once("value"),
  //     ])
  //       .then((results) => {
  //         const { data } = results[0];
  //         const meta = results[1].val();
  //         resolve({ ...data, ...data.metadata, ...meta });
  //       })
  //       .catch((error) => {
  //         reject(error);
  //       });
  //   });
  // };

  // userSerialized = (uid) => {
  //   return this.functions.httpsCallable("userFullDataSerialized")(uid);
  // };
  // authUpdate = (data) => {
  //   return this.functions.httpsCallable("authUpdate")(data);
  // };
  // userUpdate = (data) => {
  //   return this.functions.httpsCallable("userUpdate")(data);
  // };
  // userDelete = (uid) => this.functions.httpsCallable("userDelete")(uid);

  // userPosts = (userUid) =>
  //   new Promise((resolve, reject) => {
  //     this.db
  //       .ref("posts")
  //       .orderByChild("user")
  //       .equalTo(userUid)
  //       .once("value")
  //       .then((snapshot) => {
  //         const data = snapshot.val() || {};
  //         resolve(data.convertToArray((v, uid) => ({ ...v, uid })));
  //       })
  //       .catch((error) => {
  //         reject(error);
  //       });
  //   });

  // userDbRef = (uid) => this.db.ref(`users/${uid}`);

  // // subscribe = (user, target, remove = false) =>
  // //   this.functions.httpsCallable(`${remove === true ? "un" : ""}subscribe`)({
  // //     user,
  // //     target,
  // //   });

  // subscribe = (targetUser) =>
  //   this.functions.httpsCallable("subscribe")(targetUser);
  // unsubscribe = (subscription) =>
  //   this.functions.httpsCallable("unsubscribe")(subscription);

  // userPayments = (uid) => this.cfs.collection(`users/${uid}/payments`);

  // userToken = (uid) =>
  //   this.auth.currentUser.getIdToken(/* forceRefresh */ true);

  // usersList = (options = {}) =>
  //   this.functions.httpsCallable("usersList")(options);

  // getCellsFromSquareGeoRegion = () =>
  //   this.functions.httpsCallable("getCellsFromSquareGeoRegion");

  // uploadAvatar = (file, user = "") => {
  //   return new Promise((resolve, reject) => {
  //     const userUid = user.uid || user.id || user;
  //     if (!user) reject("No user data");
  //     if (!file) reject("No file");
  //     if (file.size > 2 * 1048576) reject("File size exceeds limit in 2MB");
  //     const ref = this.storage.ref(`Avatar/${userUid}/${file.name}`);
  //     ref
  //       .put(file)
  //       .then(() => ref.getDownloadURL())
  //       .then((url) => resolve(url))
  //       .catch((error) => reject(error));
  //   });
  // };

  // // *** Post API ***

  // post = (uid) =>
  //   new Promise((resolve, reject) => {
  //     this.db
  //       .ref(`/posts/${uid}`)
  //       .once("value")
  //       .then((snapshot) => {
  //         const data = snapshot.val();
  //         if (!data) reject(`Post ${uid} didn't found`);
  //         else resolve({ ...data, uid });
  //       })
  //       .catch((error) => {
  //         reject(error);
  //       });
  //   });

  // posts = (idsFilterList) =>
  //   new Promise((resolve, reject) => {
  //     this.db
  //       .ref("posts")
  //       .once("value")
  //       .then((snapshot) => {
  //         const data = (snapshot.val() || {}).convertToArray((v, uid) => ({
  //           ...v,
  //           uid,
  //         }));
  //         resolve(
  //           idsFilterList instanceof Array
  //             ? data.filter((v) => idsFilterList.indexOf(v.uid) !== -1)
  //             : data
  //         );
  //       })
  //       .catch((error) => {
  //         reject(error);
  //       });
  //   });

  // likePost = (post) => this.functions.httpsCallable("likePost")({ post });
  // likeComment = (comment) =>
  //   this.functions.httpsCallable("likePost")({ comment });

  // report = ({ post = null, comment = null, reason }) => {
  //   return this.functions.httpsCallable("report")({ post, comment, reason });
  // };
  // reportsForPosts = () =>
  //   new Promise((resolve, reject) => {
  //     this.db
  //       .ref("/postReports")
  //       .once("value")
  //       .then((snapshot) => {
  //         const data = snapshot.val() || {};
  //         const posts = {};
  //         const users = {};
  //         const reports = [];
  //         let postsIDs = Object.keys(data);

  //         const deletePromises = [];

  //         // если в данных жалоб нет ни одного поста
  //         // сразу ресолвим пустой массив
  //         if (!postsIDs.length) resolve([]);

  //         // получение данных постов и фильтрация "неживых" постов
  //         Promise.allSettled(
  //           postsIDs.map((v) => this.db.ref(`/posts/${v}`).once("value"))
  //         ).then((results) => {
  //           results.forEach((r, i) => {
  //             if (r.status === "fulfilled" && r.value.val() !== null) {
  //               const d = r.value.val();
  //               posts[postsIDs[i]] = d;
  //               // сохранение идентификатора автора поста
  //               // как ключ в объекте users
  //               users[d.user] = true;
  //             } else {
  //               delete data[postsIDs[i]];
  //               deletePromises.push(
  //                 this.db.ref(`/postReports/${postsIDs[i]}`).set(null)
  //               );
  //             }
  //           });

  //           if (deletePromises.length) {
  //             Promise.allSettled(deletePromises);
  //           }

  //           // если после фильтрации не осталось
  //           // ни одного "живого" поста, то тоже
  //           // ресолвим пустой массив
  //           postsIDs = Object.keys(data);
  //           if (!postsIDs.length) resolve([]);

  //           // собираем идентификаторы всех жалобщиков
  //           // и сохраняем их как ключи в объекте users
  //           Object.values(data).forEach((postReports) => {
  //             Object.values(postReports).forEach((report) => {
  //               users[report.user] = true;
  //               reports.push({
  //                 ...report,
  //                 uid: report.id,
  //                 post: posts[report.postId],
  //               });
  //             });
  //           });

  //           // получение данных авторов постов и жалобщиков
  //           // по собранным идентификаторам
  //           const usersIDs = Object.keys(users);
  //           Promise.allSettled(usersIDs.map((uid) => this.user(uid))).then(
  //             (results) => {
  //               results.forEach((r, i) => {
  //                 if (r.status === "fulfilled") {
  //                   users[usersIDs[i]] = r.value;
  //                 }
  //               });
  //               // сборка окончательного рекордсета
  //               reports.forEach((report, i, a) => {
  //                 const { postId, user } = report;
  //                 const post = posts[postId];
  //                 const postText = post.text;
  //                 const author = users[post.user];
  //                 const reporter = users[report.user];
  //                 a[i] = {
  //                   ...report,
  //                   previewUrl: post.previewUrl,
  //                   author,
  //                   reporter,
  //                   postText,
  //                 };
  //               });

  //               resolve(reports);
  //             }
  //           );
  //         });
  //       })
  //       .catch((error) => {
  //         reject(error);
  //       });
  //   });

  // reportsForComments = () =>
  //   new Promise((resolve, reject) => {
  //     this.db
  //       .ref(`/commentReports`)
  //       .once("value")
  //       .then((snapshot) => {
  //         // данные всех жалоб на коментарии, сгруппированные по
  //         // идентификаторам коментариев
  //         const data = snapshot.val() || {};
  //         const posts = {};
  //         const users = {};
  //         const comments = {};
  //         let reports = [];

  //         Object.keys(data).forEach((commentId) => {
  //           const reportsForComment = data[commentId];
  //           Object.keys(reportsForComment).forEach((reportId) => {
  //             const report = reportsForComment[reportId];
  //             const { postId, commentId, user } = report;

  //             // полученные данные сохраняем в жалобах
  //             reports.push({ ...report, uid: reportId });

  //             // а идентификатор жалобщика заносим в коллекцию users
  //             users[user] = true;

  //             // формируем массив промисов для получения данных
  //             // коментариев по идентификаторам коментария и поста
  //             // promises.push(this.postComment(postId, commentId));
  //             comments[commentId] = postId;
  //           });
  //         });

  //         const commentsIDs = Object.keys(comments);
  //         // получаем данные комментариев, на которые ссылаются жалобы
  //         Promise.allSettled(
  //           commentsIDs.map((c) => this.postComment(comments[c], c))
  //         ).then((results) => {
  //           results.forEach((r, i, a) => {
  //             const uid = commentsIDs[i];
  //             if (r.status === "fulfilled") {
  //               const { post, postId = post, user } = r.value;
  //               // если запрос выполнен успешно, то его результат
  //               // сохраняем в объекте комментарие
  //               comments[uid] = { ...r.value, uid };
  //               // а идентификаторы поста, к которому относится комментарий,
  //               // и автора комментария
  //               // сохраняем ключами в соответствующих коллекциях
  //               posts[postId] = true;
  //               users[user] = true;
  //             } else {
  //               comments[uid] = null;
  //             }
  //           });

  //           // получаем данные постов, к которым относятся комментарии,
  //           // на которые поступили жалобы
  //           const postIds = Object.keys(posts);
  //           Promise.allSettled(
  //             postIds.map((uid) => this.db.ref(`/posts/${uid}`).once("value"))
  //           ).then((results) => {
  //             results.forEach((r, i, a) => {
  //               const uid = postIds[i];
  //               if (r.status === "fulfilled" && r.value.val() !== null) {
  //                 // если запрос выполнен успешно,
  //                 // то сохраняем данные поста
  //                 const v = r.value.val();
  //                 posts[uid] = { ...v, uid };
  //                 // а идентификатор автора поста добавляем в коллекцию users
  //                 users[v.user] = true;
  //               } else {
  //                 posts[uid] = null;
  //               }
  //             });

  //             const userIds = Object.keys(users);
  //             Promise.allSettled(userIds.map((u) => this.user(u))).then(
  //               (results) => {
  //                 results.forEach((r, i, a) => {
  //                   const uid = userIds[i];
  //                   if (r.status === "fulfilled") {
  //                     users[uid] = { ...r.value, uid };
  //                   } else {
  //                     users[uid] = null;
  //                   }
  //                 });

  //                 const deletePromises = [];
  //                 reports = reports.filter((r) => {
  //                   const exists =
  //                     comments[r.commentId] !== null &&
  //                     posts[r.postId] !== null &&
  //                     users[r.user] !== null;
  //                   if (!exists) {
  //                     deletePromises.push(
  //                       this.db.ref(`/commentReports/${r.commentId}`).set(null)
  //                     );
  //                   }
  //                   return exists;
  //                 });
  //                 reports = reports.map((r) => {
  //                   const { postId, commentId } = r;
  //                   const post = posts[postId];
  //                   const comment = comments[commentId];
  //                   return {
  //                     ...r,
  //                     postAuthor: users[post.user],
  //                     postPreviewUrl: post.previewUrl,
  //                     postText: post.text,
  //                     commentText: comment.text,
  //                     commentAuthor: users[comment.user],
  //                     reporter: users[r.user],
  //                     post,
  //                     comment,
  //                   };
  //                 });

  //                 if (deletePromises.length) {
  //                   Promise.allSettled(deletePromises);
  //                 }
  //                 resolve(reports);
  //               }
  //             );
  //           });
  //         });
  //       })
  //       .catch((error) => {
  //         {
  //           reject(error);
  //         }
  //       });
  //   });

  // deleteReport = (data) => this.functions.httpsCallable("deleteReport")(data);

  // createPost = (data) => this.functions.httpsCallable("createPost")(data);
  // updatePost = (data) => this.functions.httpsCallable("updatePost")(data);
  // deletePost = (uid) => this.functions.httpsCallable("deletePost")(uid);
  // banPost = (uid, disabled = true) => {
  //   const data = { postId: uid, disabled };
  //   return this.functions.httpsCallable("banPost")(data);
  // };

  // createComment = (text, postId) =>
  //   this.functions.httpsCallable("createComment")({ text, postId });
  // updateComment = (postId, id, text) =>
  //   this.functions.httpsCallable("updateComment")({ postId, id, text });
  // deleteComment = (postId, id) =>
  //   this.functions.httpsCallable("deleteComment")({ postId, id });

  // postComments = (postId) =>
  //   new Promise((resolve, reject) => {
  //     this.db
  //       .ref(`/comments/${postId}`)
  //       .once("value")
  //       .then((snapshot) => {
  //         const data = snapshot.val() || {};
  //         resolve(
  //           data.convertToArray((v, uid) => ({
  //             ...v,
  //             uid,
  //           }))
  //         );
  //       })
  //       .catch((error) => {
  //         reject(error);
  //       });
  //   });
  // postComment = (postId, commentId) =>
  //   new Promise((resolve, reject) => {
  //     this.db
  //       .ref(`/comments/${postId}/${commentId}`)
  //       .once("value")
  //       .then((snapshot) => {
  //         const data = snapshot.val();
  //         if (data) {
  //           resolve({ ...data, uid: commentId });
  //         } else {
  //           reject(`Comment ${commentId} for post ${postId} not exists`);
  //         }
  //       })
  //       .catch((error) => {
  //         reject(error);
  //       });
  //   });

  // // *** Lots API ***

  // lots = () => this.db.ref("lots");

  // lotByUid = (uid) => {
  //   return this.functions.httpsCallable("lotByUid")(uid);
  // };
  // lotByCoords = (coords) => {
  //   return this.functions.httpsCallable("getLot")(coords);
  // };
  // lotsByCoordsFast = (coords) => {
  //   return this.functions.httpsCallable("getLotsCluster")(coords);
  // };
  // lotsByCoords = (coords) => {
  //   return this.functions.httpsCallable("getLots")(coords);
  // };

  // lotsByUser = () => this.functions.httpsCallable("userlots")();

  // buyLot = (lotid) => this.functions.httpsCallable("buy")(lotid);

  // rentLot = ({ lotid, period }) =>
  //   this.functions.httpsCallable("rent")({ lotid, period });

  // setLotStatus = ({ lotid, status, userPrice = 0, rentPrice = 0 }) =>
  //   this.functions.httpsCallable("changeStatus")({
  //     lotid,
  //     status,
  //     userPrice,
  //     rentPrice,
  //   });

  // cellsByCoord = (coords) => this.getCellsFromSquareGeoRegion()(coords);

  // lotsWithStructures = () => this.db.ref("lots").orderByKey();

  // // *** Storage API *** //

  // getStorageList = (path) => {
  //   const storageRef = this.storage.ref();
  //   const listRef = storageRef.child(path);
  //   return listRef.listAll();
  // };

  // getStructureScreenshots = (str_uid) => {
  //   return new Promise((resolve, reject) => {
  //     const storageRef = this.storage.ref();
  //     // Create a reference under which you want to list
  //     const listRef = storageRef.child(`screenshots/${str_uid}`);

  //     // Find all the prefixes and items.
  //     listRef
  //       .listAll()
  //       .then(function (res) {
  //         res.prefixes.forEach(function (folderRef) {
  //           console.log(folderRef);
  //           // All the prefixes under listRef.
  //           // You may call listAll() recursively on them.
  //         });
  //         res.items.forEach(function (itemRef) {
  //           resolve(itemRef.getDownloadURL());
  //           // All the items under listRef.
  //         });
  //       })
  //       .catch(function (error) {
  //         console.error(error);
  //       });
  //   });
  // };
}

export default new Firebase();

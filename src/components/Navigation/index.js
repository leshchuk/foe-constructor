import React from "react";
import { Nav } from "react-bootstrap";

import "./Navigation.css";

let count = 0;

function Navigation(props = {}) {
    const {activeKey} = props;

    console.log("NAVIGATION", count++)

    return (<nav className="side_nav">
      <Nav defaultActiveKey="/" activeKey={activeKey} className="flex-column">
        <Nav.Link href="/">Конструктор</Nav.Link>
        <Nav.Link href="/epoch">Эпохи</Nav.Link>
        <Nav.Link href="/categories">Категории зданий</Nav.Link>
        <Nav.Link href="/buildings">Здания</Nav.Link>
      </Nav>
    </nav>);
}


export default React.memo(Navigation);
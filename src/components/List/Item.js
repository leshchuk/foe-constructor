import React, { useState, useContext } from 'react';
import { Button, ButtonGroup, ButtonToolbar } from "react-bootstrap";

import { DataSourceContext } from "../../components/DataSource";
import ListItemForm from "./ItemForm";

import { DICS } from "../../tools/constants";

function ListItem(props) {
  const {record, dataModel, dataPath} = props;

  const [showForm, setShowForm] = useState(false);
  const [buisy, setBuisy] = useState(false);
  const datasource = useContext(DataSourceContext);

  function onSubmit(data) {
    setBuisy(true);
    datasource.update(`${dataPath}/${record.uid}`, data);
    setBuisy(false);
    setShowForm(false);
  }

  function onDelete() {
    setBuisy(true);
    datasource.delete(`${dataPath}/${record.uid}`);
    setBuisy(false);
  }

  return <div className="my-1 p-1 pl-3 border border-secondary rounded">
    <ButtonToolbar className="justify-content-between align-items-center btn-toolbar">
      <div>{dataModel.getListText(record)}</div>
      <ButtonGroup>
        <Button variant="secondary" onClick={() => {setShowForm(!showForm)}}>Edit</Button>
        <Button variant="danger" onClick={onDelete}>Delete</Button>
      </ButtonGroup>
    </ButtonToolbar>
    <ListItemForm fields={dataModel.getFormConfig(record)} show={showForm} disabled={buisy} onSubmit={onSubmit} />
  </div>
}

export default ListItem;

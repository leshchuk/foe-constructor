import React, { useState, useEffect, useContext } from 'react';

import { Alert } from "react-bootstrap";
import { v1 as uuidv1 } from "uuid";

import { DataSourceContext } from "../../components/DataSource";

import ListHeader from "./Header";
import ListItem from "./Item";
import ListItemForm from "./ItemForm";

import "./List.css";

function List(props) {
  const {dataPath, dataModel, title} = props;

  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [openAddForm, setOpenAddForm] = useState(false)

  const datasource = useContext(DataSourceContext);

  const onDataSnapshot = function(snap) {
    let data = snap.val() || {};
    data = Object.values(data);
    data.sort((a, b) => parseInt(a.order || 0, 10) - parseInt(b.order || 0, 10));
    setData(data);
    setLoading(false);
  }
  const onDataError = function(error) {
    setError(`${error.code}: ${error.message}`);
    setLoading(false);
  }
  const onAdd = function(data) {
    datasource.add(dataPath, data)
  }

  useEffect(() => {
    setLoading(true);
    setError(null);
    datasource.subscribe(dataPath, onDataSnapshot, onDataError);
    return () => {
      datasource.unsubscribe(dataPath);
    };
  }, [datasource, dataPath]);

  return (<div className="editable_list">
    <ListHeader text={title} onAdd={() => setOpenAddForm(!openAddForm)} />
    <ListItemForm 
      onSubmit={onAdd} 
      show={openAddForm} 
      fields={dataModel.getFormConfig()} 
      className="border rounded my-1 p-1" />
    {data.length > 0 && !loading && <>
      {data.map((entry) => <ListItem key={uuidv1()} dataPath={dataPath} dataModel={dataModel} record={entry} />)}
    </>}
    {data.length === 0 && !loading && <Alert variant="info">No data</Alert>}
    {loading && <Alert variant="warn">Loading...</Alert>}
    {error !== null && <Alert variant="danger">{error}</Alert>}
  </div>);
}

export default List;

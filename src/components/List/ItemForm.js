import React, { useRef } from 'react';

import { Button, ButtonGroup, Form, FormControl, InputGroup, Collapse } from "react-bootstrap";
import { v1 as uuidv1 } from "uuid";

import { EMPTY_FN } from "../../tools/constants";

function ListItemForm(props) {
  const {fields = [], onSubmit = EMPTY_FN, show = false, disabled = false, className = ""} = props;

  const formRef = useRef(null);

  function submit() {
    if (disabled) return;
    const data = {};
    [...formRef.current.getElementsByTagName("input")].forEach(f => {
      const {name, value} = f;
      data[name] = value;
    })
    onSubmit(data);
  }

  return <Collapse in={show}>
    <Form className={className} ref={formRef}>
      {fields.map(f => {
        const {
          name, 
          label, 
          value = "",
          type = "text"
        } = f;
        return <InputGroup key={uuidv1()} className="my-1">
          {!!label && <InputGroup.Prepend>
            <span className="input-group-text">{label}</span>
          </InputGroup.Prepend>}
          <FormControl name={name} defaultValue={value} type={type}></FormControl> 
        </InputGroup>
      })}
      <ButtonGroup>
        <Button type="button" onClick={submit}>Submit</Button>
      </ButtonGroup>
    </Form>
  </Collapse>;
}

export default ListItemForm;
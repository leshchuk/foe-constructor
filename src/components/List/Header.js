import React from 'react';

import { Button, ButtonGroup, ButtonToolbar } from "react-bootstrap";

import { EMPTY_FN } from "../../tools/constants";

function ListHeader(props) {
  const {
    text, 
    onAdd = EMPTY_FN, 
    onDeleteAll = EMPTY_FN
  } = props;

  return <ButtonToolbar className="justify-content-between align-items-center">
    <h1>{text}</h1>
    <ButtonGroup>
      <Button variant="primary" onClick={onAdd}>Add</Button>
      <Button variant="danger" onClick={onDeleteAll} disabled={true}>Delete all</Button>
    </ButtonGroup>
  </ButtonToolbar>
}

export default ListHeader;
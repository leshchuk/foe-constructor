import { CommonObject, IModel, IModelField } from "../tools/interfaces";
import { convertValueByType as convert } from "../tools/utils";

class Model {
  table: string
  fields: IModelField[]
  template: string

  constructor({table, fields = [], template = ""}: IModel) {
    this.table = table;
    this.fields = fields;
    this.template = template;
  }

  /**
   * Формирует объект конфигурации формы на основе массива полей формы.
   * Опционально устанавливает начальные значения полей.
   * 
   * @param inital CommonObject опциональный объект с инициализирующими значениями полей формы:
   * ключи объекта соответствуют именам полей формы, значения - значениям полей
   * 
   * @returns IModelField[] 
   */
  getFormConfig(inital?: CommonObject): IModelField[] {
    let data: IModelField[] = [...this.fields]
    if (inital) {
      data = data.map(f => ({...f, value: convert(inital[f.name], f.type)}));
    }
    return data;
  }

  /**
   * 
   * @param record CommonObject объект записи данных
   */
  getListText(record: CommonObject): string {
    let text: string = "" + this.template;
    const rx: RegExp = /\{([^}]+)\}/;
    let match: RegExpMatchArray | null = text.match(rx);
    while (match) {
      const field = match[1];
      text = text.replace(new RegExp(`{${field}}`, "g"), record[field] || "");
      match = text.match(rx);
    }
    return text;
  }
}

export default Model;

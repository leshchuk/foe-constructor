import Model from "./Model";

const fields = [
  {name: "name_en", label: "Название (en)", type: "text"},
  {name: "name_ru", label: "Название (ru)", type: "text"},
  {name: "order", label: "Порядок сортировки", type: "number"},
]
const record = {
  name_en: "Stone Age",
  name_ru: "Каменный век",
  order: 1,
  uid: "test"
}

test('Class Model test', () => {
  const model = new Model({
    fields,
    template: "{order}. {name_ru} [{name_en}]"
  });

  expect(model).toBeInstanceOf(Model);

  {
    const config = model.getFormConfig();
  
    expect(config).toBeInstanceOf(Array);
  
    for (let i = 0; i < 3; i++) {
      const field = config[i];
      expect(field).toHaveProperty("name");
      expect(field).toHaveProperty("label");
      expect(field).toHaveProperty("type");
    }
    expect(config[0].value).toBeUndefined();
    expect(config[1].value).toBeUndefined();
    expect(config[2].value).toBeUndefined();
  }

  {
    const config = model.getFormConfig(record);
  
    expect(config).toBeInstanceOf(Array);
  
    for (let i = 0; i < 3; i++) {
      const field = config[i];
      expect(field).toHaveProperty("name");
      expect(field).toHaveProperty("label");
      expect(field).toHaveProperty("type");
    }
    expect(config[0].value).toEqual(record.name_en);
    expect(config[1].value).toEqual(record.name_ru);
    expect(config[2].value).toEqual(record.order);

    const text = model.getListText(record);
    expect(text).toEqual("1. Каменный век [Stone Age]")
  }
});

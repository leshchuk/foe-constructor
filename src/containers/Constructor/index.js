import React from 'react';

import { Container, Row, Col } from "react-bootstrap";

import Navigation from "../../components/Navigation";

function Constructor(props = {}) {
  return (<Container>
    <Row>
      <Col sm={2}>
        <Navigation activeKey="/" />
      </Col>
      <Col sm={10}>
        <main className="main_area">
          Main content area
        </main>
      </Col>
    </Row>
  </Container>);
}

export default Constructor;
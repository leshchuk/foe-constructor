import React from 'react';

import { Container, Row, Col } from "react-bootstrap";

import Navigation from "../../components/Navigation";
import List from "../../components/List";

import Model from "../../models/Model";
const categoryModel = new Model({
  fields: [
    {name: "name_en", label: "Название (en)", type: "text"},
    {name: "name_ru", label: "Название (ru)", type: "text"},
    {name: "order", label: "Порядок сортировки", type: "number"},
  ],
  template: "{order}. {name_ru} [{name_en}]"
});

function Categories() {
  return (<Container>
    <Row>
      <Col sm={2}>
        <Navigation activeKey="/categories" />
      </Col>
      <Col sm={10}>
        <main className="main_area">
          <List title="Категории зданий" dataPath="categories" dataModel={categoryModel} />
        </main>
      </Col>
    </Row>
  </Container>);
}

export default (Categories);

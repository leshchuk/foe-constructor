import React from 'react';

import { Container, Row, Col } from "react-bootstrap";

import Navigation from "../../components/Navigation";

function Buildings(props = {}) {
  return (<Container>
    <Row>
      <Col sm={2}>
        <Navigation activeKey="/buildings" />
      </Col>
      <Col sm={10}>
        <main className="main_area">
          Buildings list
        </main>
      </Col>
    </Row>
  </Container>);
}

export default Buildings;
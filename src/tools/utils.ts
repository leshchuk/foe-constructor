import { ModelFieldType } from "./interfaces";

export const convertValueByType = function(value: any, type?: ModelFieldType): any {
  if (type === ModelFieldType.number) return value && !isNaN(value) ? parseFloat(value) : 0;
  if (type === ModelFieldType.int) return value && !isNaN(value) ? parseInt(value, 10) : 0;
  return value ? String(value) : "";
}
  
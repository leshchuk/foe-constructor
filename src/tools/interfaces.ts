export interface CommonObject {
  [key: string]: any
}

export enum ModelFieldType {
  text = "text",
  number = "number",
  int = "int",
  // datetime = "datetime",
  // date = "date",
  // time = "time",
  // select = "select",
  // multiselect = "multiselect",
  // check = "check",
  // radio = "radio",
  // fkey = "fkey",
}
export interface IModelField {
  name: string,
  label?: string,
  required?: boolean,
  type?: ModelFieldType,
  reference?: string,
  value?: string | number
}
export interface IModel {
  table: string,
  fields?: Array<IModelField>,
  template?: string
}

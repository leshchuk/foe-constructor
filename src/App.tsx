import React from 'react';
import { Container } from "react-bootstrap";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Constructor from "./containers/Constructor";
import Buildings from "./containers/Buildings";
import Epoch from "./containers/Epoch";
import Categories from "./containers/Categories";

import './App.css';

function App() {
  return (
    <div className="App">
      <Container>
        <header className="my-4">
          <h1>FoE Constructor</h1>
        </header>
      </Container>
      <Router>
        <Switch>
          <Route path="/epoch">
            <Epoch />
          </Route>
          <Route path="/buildings">
            <Buildings />
          </Route>
          <Route path="/categories">
            <Categories />
          </Route>
          <Route path="/">
            <Constructor />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
